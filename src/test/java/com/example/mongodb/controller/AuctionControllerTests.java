package com.example.mongodb.controller;

import com.example.mongodb.entity.Auction;
import com.example.mongodb.repository.AuctionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static java.util.Optional.ofNullable;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AuctionController.class)
public class AuctionControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AuctionRepository repository;

    Auction auction;

    @Before
    public void setup() {
        auction = new Auction(
                "1",
                "Danillo",
                "Guitar"
        );

    }
    @Test
    public void getAuction_givenAuctionId_shouldReturnAuction() throws Exception {
        when(repository.findById("1")).thenReturn(ofNullable(auction));

        this.mockMvc.perform(get("/mongo/auction/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(auction.getId()))
                .andExpect(jsonPath("consignorName").value(auction.getConsignorName()))
                .andExpect(jsonPath("lotName").value(auction.getLotName()));
    }

    @Test
    public void createAuction_givenAuctionId_shouldCreateAuction() throws Exception {
        mockMvc.perform(post("/mongo/auction")
                .content(asJsonString(new Auction( "Danillo", "Guitar")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void deleteAuction_givenAuctionId_shouldDeleteAuction() throws Exception {
        mockMvc.perform(delete("/mongo/auction/1"))
                .andExpect(status().isOk());
    }



    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

