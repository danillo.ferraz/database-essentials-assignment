package com.example.mongodb.controller;

import com.example.mongodb.entity.Auction;
import com.example.mongodb.repository.AuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AuctionController {

    @Autowired
    AuctionRepository repository;

    @GetMapping("/mongo/auction")
    public ResponseEntity<List<Auction>> getAllAuction() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(repository.findAll());
    }
    @GetMapping("/mongo/auction/{id}")
    public ResponseEntity<Auction> getAuction(@PathVariable String id) {
        Optional<Auction> res = repository.findById(id);
        if(res.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(res.get());
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/mongo/auction/name/{name}")
    public ResponseEntity<List<Auction>> getAuctionByName(@PathVariable String name) {
        Optional<List<Auction>> res = repository.findByConsignorName(name);
        if(res.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(res.get());
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PostMapping("/mongo/auction")
    public ResponseEntity createAuction(@RequestBody Auction auction) {
        repository.save(auction);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/mongo/auction/{id}")
    public ResponseEntity deleteAuction(@PathVariable String id) {
        repository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @DeleteMapping("/mongo/auction")
    public ResponseEntity deleteAll() {
        repository.deleteAll();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
