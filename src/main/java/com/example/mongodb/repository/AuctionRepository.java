package com.example.mongodb.repository;

import com.example.mongodb.entity.Auction;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface AuctionRepository extends MongoRepository<Auction, String> {

    public Optional<List<Auction>> findByConsignorName(String consignorName);
}
