package com.example.mongodb.entity;

import org.springframework.data.annotation.Id;

public class Auction {

    @Id
    public String id;
    public String consignorName;
    public String lotName;

    public Auction() {
    }

    public Auction(String consignorName, String lotName) {
        this.consignorName = consignorName;
        this.lotName = lotName;
    }

    public Auction(String id, String consignorName, String lotName) {
        this.id = id;
        this.consignorName = consignorName;
        this.lotName = lotName;
    }

    public String getId() {
        return id;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public String getLotName() {
        return lotName;
    }

    @Override
    public String toString() {
        return "Auction{" +
                "id='" + id + '\'' +
                ", consignorName='" + consignorName + '\'' +
                ", lotName='" + lotName + '\'' +
                '}';
    }
}
